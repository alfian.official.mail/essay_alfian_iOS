//: Playground - noun: a place where people can play

import UIKit

/*
 * Question 1
 * Answer:
 * Code will produce error because because willThrowAnError function explicitly not add throws, but when throws added will produce `Code was busted!`
 * because function `willThrowAnError` throws error type `busted`
 */
enum MyError: Error {
    case broken
    case busted
    case badgered
}

func willThrowAnError() throws {
    throw MyError.busted
}

do {
    try willThrowAnError()
} catch MyError.busted {
    print("Code was busted!")
} catch {
    print("Code just didn't work")
}

/*
 * Question 2
 * Answer:
 * `reserveCapacity` based on apple documentation
 * This method ensures that the array has unique, mutable, contiguous storage, with space allocated for at least the requested number of elements.
 */
var names = [String]()
names.reserveCapacity(2)
names.append("Amy")
names.append("Rory")
names.append("Clara")

/*
 * Question 3
 * Answer:
 * `String...` called as Variadic Tuple or Variadic Parameter
 * `joined` will transform Array String to String with defined separator
 * Code will produce `Criminal masterminds: Amy, Rory, Clara`
 */
func greet(name: String...) {
    print("Criminal masterminds:", names.joined(separator: ", "))
}
greet(name: "Malcolm", "Kaylee", "Zoe")

/*
 * Question 4
 * Answer:
 * First `!` is Logical Operator NOT = inverts a Boolean value so that true becomes false
 * Second `!` is force unwraping for Optional value whitout checking value is nil or not
 * This code will print `Message one` because NOT userLoggedIn is `true`
 */
let userLoggedIn: Bool? = false
if !userLoggedIn! {
    print("Message one")
} else {
    print("Message two")
}

/*
 * Question 5
 * Answer:
 * `Switch` is Pattern Matching
 * In this `Switch` we use tuple `number % 3 == 0` and `number % 5 == 0`
 * `%` is Operator Remainder,
 * `number % 3 == 0` number divide by 3 and remain 0 will produce true
 * `number % 5 == 0` number divide by 5 and remain 0 will produce true
 * So when number is 15 divide by 3 = 5 and no remain so result is `true`
 * when number is 15 divide by 5 = 3 and no remain so result is `true`
 * Switch will call third case that value is `(true, true)`
 * and printed `FizzBuzz`
 */
func fizzbuzz(number: Int) -> String {
    switch (number % 3 == 0, number % 5 == 0) {
    case (true, false):
        return "Fizz"
    case (false, true):
        return "Buzz"
    case (true, true):
        return "FizzBuzz"
    default:
        return String(number)
    }
}
print(fizzbuzz(number: 15))

/*
 * Question 6
 * Answer:
 * names is Dictionary with type [String : String]
 * `??` is nil coalescing operator or default operator
 * because on names dictionary there is no `doctor` key that make doctor value use default variable `Bones`
 */
let names6 = ["Pilot": "Wash", "Doctor" : "Simon"]
let doctor = names6["doctor"] ?? "Bones"
print(doctor)

/*
 * Question 7
 * Answer:
 * Set is different with Array,
 * Arrays are effectively ordered lists and are used to store lists of information in cases where order is important.
 * Sets are different in the sense that order does not matter and these will be used in cases where order does not matter.
 * Sets are especially useful when you need to ensure that an item only appears once in the set.
 * thats make this code will produce count `3` because we are not reinsert same String twice with `Set`
 */
var spaceship = Set<String>()
spaceship.insert("Serenity")
spaceship.insert("Enterprise")
spaceship.insert("TARDIS")
spaceship.insert("Serenity")
print(spaceship.count)

/*
 * Question 8
 * Answer:
 * `NSMutableDictionary` is mutable (`Dictionary`/`NSDictionary`)
 * On swift we can use Dictionary with var for Mutable Dictionary and use let for immutable Dictionary
 * `setValue` will add new element to crew and will produce count 1
 */
let crew = NSMutableDictionary()
crew.setValue("Kryten", forKey: "Mechanoid")
print(crew.count)

/*
 * Question 9
 * Answer:
 * Map is functional programing: Function that return Function
 * map will transform each Int multiply by 10
 * Result: [10, 30, 50, 70, 90]
 */
let numbers = [1, 3, 5, 7, 9]
let result = numbers.map({ $0 * 10 })
print(result)

/*
 * Question 10
 * Answer:
 * 0x indicate that value is hexadecimal or base16
 * 0x10 == 16 decimal
 */
let foo = 0x10
print(foo)

/*
 * Question 11
 * Answer:
 * Struct is immutable for mutable properties we need set function as `mutating`
 * will produce error `Cannot assign to property: 'self' is immutable`
 * because function not set as mutable
 */
struct Spaceship {
    var name: String
    
    mutating func setName(_ newName: String) {
        name = newName
    }
}
var enterprise = Spaceship(name: "Enterprise")
enterprise.setName("Enterprise A")
print(enterprise.name)

/*
 * Question 12
 * Answer:
 * `possibleNumber` is String
 * Int(_ source: String) for convert String to Int and when Error converting return nil
 * result: 1701 type is Int
 */
let possibleNumber = "1701"
let convertedNumber = Int(possibleNumber)

/*
 * Question 13
 * Answer:
 * Pattern Matcing with tuple
 * `fallthrough` if match skip this case to next case
 * result: `Crew`
 */
let name = "Simon"
switch name {
case "Simon":
    fallthrough
case "Malcom", "Zoe", "Kaylee":
    print("Crew")
default:
    print("Not crew")
}

/*
 * Question 14
 * Answer:
 * Filter is one of functional programming implementation
 * $0 >= 5 will filter value that higher and equal 5
 * result: [5, 7, 9]
 */
let numbers14 = [1, 3, 5, 7, 9]
let result14 = numbers14.filter({ $0 >= 5 })


/*
 * Question 15
 * Answer:
 * no need to overide init because init not implemented before
 * `initialName` will set name
 * result: `Serenity`
 */
class Starship15 {
    var name: String
    
    init(initialName: String) {
        name = initialName
    }
}
let serenity15 = Starship15(initialName: "Serenity")
print(serenity15.name)

/*
 * Question 16
 * Answer:
 * 0..<10 is Range will produce [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
 * result: 10
 */
let numbers16 = Array(0..<10)
print(numbers16.count)

/*
 * Question 17
 * Answer:
 * `init(repeating:count:)` = Creates a new collection containing the specified number of a single, repeated value.
 * result: ["Malkovitch", "Malkovitch"]
 */
let people = [String](repeating: "Malkovitch", count: 2)
print(people)

/*
 * Question 18
 * Answer:
 * `point` is tuple (Int, Int)
 * (556, 0) will match to first case where first tuple as bind value and second tuple as pattern matching equal to 0
 * result `X was 556`
 */
let point = (556, 0)
switch point {
case (let x, 0):
    print("X was \(x)")
case (0, let y):
    print("Y was \(y)")
case let (x, y):
    print("X was \(x) and Y was \(y)")
}

/*
 * Question number 19
 * Answer:
 * Map will transform [1, 2, 3] to [[1, 1], [2, 2], [3, 3]]
 */
let numbers19 = [1, 2, 3].map({ [$0, $0] })

/*
 * Question 20
 * Answer:
 * counts from the start point up to by excluding the `to` parameter
 * stride(from: 1, to: 17, by: 4) will produce [1, 5, 9, 13]
 * Strideable: Conforming types are notionally continuous, one-dimensional values that can be offset and measured.
 * result:
 * 1
 * 5
 * 9
 * 13
 */
for i in stride(from: 1, to: 17, by: 4) {
    print(i)
}
